package handler

import (
	"gitlab.com/stopdiiacity/stopdiiacity.netlify.app/verify"
	"io/ioutil"
	"net/http"
)

// VERSION 1.0.7.5
func Handler(w http.ResponseWriter, r *http.Request) {
	for key, value := range verify.Headers() {
		w.Header().Add(key, value)
	}

	var content, err = ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	var response = verify.Verify(content)
	w.WriteHeader(response.StatusCode)
	w.Write([]byte(response.Body))
}
